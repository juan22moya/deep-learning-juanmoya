from __future__ import print_function
import keras
from keras.datasets import cifar10
from keras.models import load_model
import os
import numpy as np
from keras.preprocessing import image


import matplotlib.pyplot as plt

from io import BytesIO
from PIL import Image
from PIL import ImageOps

import cv2

ruta=('images/ocho.png')

img = image.load_img(ruta)
print (img)
image = Image.fromarray(np.uint8(img))
image = image.resize((28, 28))
print (image)
resize_img = np.asarray(image)

# mnist category label name
mnist_labels = np.array([
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9'])

# use CNN
model = load_model('saved_models/keras_mnist_trained_model.h5')

im= cv2.imread(ruta, 0)
im = cv2.resize(im,  (28,28))
im.reshape((28,28,1))
batch = np.expand_dims(im,axis=0)
batch = np.expand_dims(batch,axis=3)

def convertCIFER10Data(image):
    img = image.astype('float32')
    img /= 255
    c = np.zeros(28 * 28 *1).reshape((1, 28, 28,1))
    c[0] = img
    return c

#data = convertCIFER10Data(resize_img)


plt.imshow(img)
plt.axis('off')

ret = model.predict(batch, batch_size=1)
# print(ret)
print("")
print("----------------------------------------------")
bestnum = 0.0
bestclass = 0
for n in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
    print("[{}] : {}%".format(mnist_labels[n], round(ret[0][n] * 100, 2)))
    if bestnum < ret[0][n]:
        bestnum = ret[0][n]
        bestclass = n

print("----------------------------------------------")

plt.show()
print("Probabilidad : {}%".format(round(bestnum * 100, 2)))
print("Prediccion: [{}].".format(mnist_labels[bestclass]))
