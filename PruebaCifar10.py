# https://github.com/uchidama/CIFAR10-Prediction-In-Keras/blob/master/keras_cifar10_webcam_prediction.ipynb

from __future__ import print_function
import keras
from keras.datasets import cifar10
from keras.models import load_model
import os
import numpy as np
from keras.preprocessing import image


import matplotlib.pyplot as plt

from io import BytesIO
from PIL import Image
from PIL import ImageOps

img = image.load_img('images/ship.jpg')

image = Image.fromarray(np.uint8(img))
image = image.resize((32, 32))

resize_img = np.asarray(image)

# cifar10 category label name
cifar10_labels = np.array([
    'airplane',
    'automobile',
    'bird',
    'cat',
    'deer',
    'dog',
    'frog',
    'horse',
    'ship',
    'truck'])

# use CNN
model = load_model('saved_models/keras_cifar10_trained_model.h5')


def convertCIFER10Data(image):
    img = image.astype('float32')
    img /= 255
    c = np.zeros(32 * 32 * 3).reshape((1, 32, 32, 3))
    c[0] = img
    return c


data = convertCIFER10Data(resize_img)

plt.imshow(img)
plt.axis('off')

ret = model.predict(data, batch_size=1)
# print(ret)
print("")
print("----------------------------------------------")
bestnum = 0.0
bestclass = 0
for n in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
    print("[{}] : {}%".format(cifar10_labels[n], round(ret[0][n] * 100, 2)))
    if bestnum < ret[0][n]:
        bestnum = ret[0][n]
        bestclass = n

print("----------------------------------------------")

plt.show()
print("Probabilidad : {}%".format(round(bestnum * 100, 2)))
print("Prediccion: [{}].".format(cifar10_labels[bestclass]))
