from flask import Flask,render_template

app = Flask(__name__)

@app.route('/')
def saluda(nombre=None, methods=['GET', 'POST']):
	return render_template("template1.html",nombre=nombre)

#Si no se define el parámetro host, flask sólo será visible desde localhost
app.run(host='localhost')
#app.run(host='0.0.0.0')