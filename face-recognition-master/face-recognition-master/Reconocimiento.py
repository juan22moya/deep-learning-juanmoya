import cv2
import numpy as np
from align import AlignDlib
from model import create_model
import os.path
from sklearn.preprocessing import LabelEncoder
import pickle
import matplotlib.pyplot as plt

class IdentityMetadata():
    def __init__(self, base, name, file):
        # dataset base directory
        self.base = base
        # identity name
        self.name = name
        # image file name
        self.file = file

    def __repr__(self):
        return self.image_path()

    def image_path(self):
        return os.path.join(self.base, self.name, self.file)

def load_metadata(path):
    metadata = []
    for i in sorted(os.listdir(path)):
        for f in sorted(os.listdir(os.path.join(path, i))):
            # Check file extension. Allow only jpg/jpeg' files.
            ext = os.path.splitext(f)[1]
            if ext == '.jpg' or ext == '.jpeg':
                metadata.append(IdentityMetadata(path, i, f))
    return np.array(metadata)

def load_image(path):
    img = cv2.imread(path, 1)
    # OpenCV loads images with color channels
    # in BGR order. So we need to reverse them
    return img[..., ::-1]

def align_image(img):
    alignment = AlignDlib('models/landmarks.dat')
    return alignment.align(96, img, alignment.getLargestFaceBoundingBox(img),
                           landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)

def detect(path):
    nn4_small2_pretrained = create_model()
    nn4_small2_pretrained.load_weights('weights/nn4.small2.v1.h5')
    metadata = load_metadata('images')
    targets = np.array([m.name for m in metadata])
    encoder = LabelEncoder()
    encoder.fit(targets)
    img1 = load_image(path)
    img = align_image(img1)
    img = (img / 255.).astype(np.float32)
    embedded = np.zeros((1, 128))
    embedded= nn4_small2_pretrained.predict(np.expand_dims(img, axis=0))[0]
    filenameknn = 'knn.sav'
    filenamesvc = 'svc.sav'
    svc = pickle.load(open(filenamesvc,'rb'))
    example_prediction = svc.predict([embedded])
    example_identity = encoder.inverse_transform(example_prediction)[0]
    plt.imshow(img1)
    plt.title(f'Recognized as {example_identity}')
    #plt.show()
    print(example_identity)
    return example_identity

#path='Juan_Moya/Juan_Moya_0003.jpg'
#nombre=detect('Ejemplos/Fernando_Sempertegui_0004.jpg')
#print(nombre)
