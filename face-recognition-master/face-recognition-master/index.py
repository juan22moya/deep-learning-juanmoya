from flask import Flask, render_template,request
import os

from Reconocimiento import detect

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)

def allowed_file(filename):
    return '.' in filename and \
      filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

# Creating simple Routes
@app.route('/test')
def test():
    return "Home Page"

@app.route('/test/about/')
def about_test():
    return "About Page"

# Routes to Render Something
@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        file = request.values['file']
        path='Ejemplos/'+file
        print(path)
        nombre=detect(path)

    return render_template("index.html")

@app.route('/about', strict_slashes=False)
def about():
    return render_template("about.html")

# Make sure this we are executing this file
if __name__ == '__main__':
    app.run(debug=True)
