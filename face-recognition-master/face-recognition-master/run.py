import os
from flask import Flask, request, redirect, url_for, send_from_directory,render_template
from werkzeug.utils import secure_filename
from Reconocimiento import detect


UPLOAD_FOLDER = "./static/img"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = './static/img'


@app.route("/")
def upload_file():

    return render_template('index.html')


@app.route("/probando", methods=['POST'])
def Probando(ruta):
    res=detect(ruta)
    return res

def allowed_file(filename):
    return '.' in filename and \
      filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route("/uploader",methods=['POST'])
def uploader():
    if request.method=="POST":
        file = request.files['file']
        print(file)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            path= r"C:\Users\Juan\PycharmProjects\face-recognition-master\face-recognition-master\static\img"
            ruta=path+"/"+str(filename)
            print(ruta)
            print("filename",filename)
            res=Probando(ruta)
            res2=str(res)
            archivo="img/"+str(filename)


            return render_template('resultado.html', nombre= res2,direccion=ruta,archivo=archivo)

# @app.route('/', methods=['GET', 'POST'])
# def upload_file():
#     if request.method == 'POST':
#      file = request.files['archivo']
#      print(file)
#      if file and allowed_file(file.filename):
#       print(file.filename)
#       filename = secure_filename(file.filename)
#       print(filename)
#       #filename = file.filename
#       print(app.config['UPLOAD_FOLDER'])
#       print(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#       file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#       return redirect(url_for('uploaded_file', filename=filename))
#     return '''
#     <!doctype html>
#     <title>Upload new File</title>
#     <h1>Upload new File</h1>
#     <form action="" method=post enctype=multipart/form-data>
#      <p><input type=file name=file>
#      <input type=submit value=Upload>
#     </form>
#     '''
#
# @app.route('/show/<filename>')
# def uploaded_file(filename):
#     filename = 'http://127.0.0.1:5000/uploads/' + filename
#     return render_template('template.html', filename=filename)
#
# @app.route('/uploads/<filename>')
# def send_file(filename):
#     return send_from_directory(UPLOAD_FOLDER, filename)

if __name__ =='__main__':
    app.run(host='192.168.100.8',debug=True,port=80)